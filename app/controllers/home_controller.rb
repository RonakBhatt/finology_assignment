class HomeController < ApplicationController
	require 'nokogiri'
	require 'mechanize'
  include ApplicationHelper
	attr_accessor :parsed_page
  caches_page :products

  def index
  end

  def products
    @products = Product.all_cached
  end

  def scrap
  	base_url = "http://magento-test.finology.com.my/"
    names    =  []
    prices   =  []
    descs    =  []
  	["women", "men"].each do |menu|
  		urls(menu).each do |url|
  			parsed_page = get_response(url)
		  	names << get_names
		  	prices << get_prices
		  	descs << get_desc
  		end
  	end
    gears_url.each do |url|
      parsed_page = get_response(url)
      names << get_names
      prices << get_prices
      descs << get_desc
    end
    names = array_parse(names)
    prices = array_parse(prices)
    descs = array_parse(descs)
    (1...names.size).each do |i|
      product = Product.find_by_name(names[i])
      if !product.present?
        product = Product.new(name: names[i])
        if descs[i].class == Array
          desc = descs[i].join("")
        else
          desc = descs[i]
        end
        product.description = desc
        product.price = prices[i].tr('$', '').to_f
        product.save
      end
    end
    expire_page action: 'index'
  	redirect_to home_products_path
  end

  private

  def get_names
  	parsed_page.css('.product-item-name>a, .product.name a>a').text.split("\n").compact.collect(&:strip).reject { |c| c.empty? }
  end

  def get_prices
  	parsed_page.css('.price-container .price').text.split("$").collect(&:strip).reject { |c| c.empty? }.map{|s| s.start_with?("$") ? s : s.prepend("$")}
  end

  def get_desc
  	base_url = "http://magento-test.finology.com.my/"
  	last_part = ".html"
  	desc = []
  	get_names.each do |name|
  		product_name = name.parameterize.to_s == "minerva-lumatech-v-tee" ? "minerva-lumatech-trade-v-tee" : name.parameterize.to_s
  		url = base_url + product_name + last_part
  		parsed_page = get_response(url)
      if parsed_page.present?
  		  desc << [parsed_page.css('.product.data.items>.item.content>.product>.value>p:first-of-type').text]
  	  else
        desc << "Not Able to fecth"
      end
    end
  	return desc
  end

  def get_response(url)
    begin
      agent = Mechanize.new()
      agent.follow_meta_refresh = true
      agent.keep_alive = false
      agent.follow_meta_refresh = true
      @parsed_page = agent.get(url).parser
    rescue
      @parsed_page = nil
    end
  end

  def urls(category_name)
    # urls = ["http://magento-test.finology.com.my/category_name/tops-category_name/jackets-category_name.html"].map {|s| s.gsub(/category_name/, category_name)}
  	urls = ["http://magento-test.finology.com.my/category_name/tops-category_name/jackets-category_name.html","http://magento-test.finology.com.my/category_name/tops-category_name/hoodies-and-sweatshirts-category_name.html","http://magento-test.finology.com.my/category_name/tops-category_name/tees-category_name.html","http://magento-test.finology.com.my/category_name/tops-category_name/tanks-category_name.html","http://magento-test.finology.com.my/category_name/bottoms-category_name/pants-category_name.html","http://magento-test.finology.com.my/category_name/bottoms-category_name/shorts-category_name.html"].map {|s| s.gsub(/category_name/, category_name)}
  	return urls
  end

  def gears_url
    ["http://magento-test.finology.com.my/gear/bags.html","http://magento-test.finology.com.my/gear/fitness-equipment.html","http://magento-test.finology.com.my/gear/watches.html"]
  end
end

